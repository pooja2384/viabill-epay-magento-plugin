<?php
/**
 * @author Rune Kok Nielsen <rune@bixel.dk>
 */
class Bixel_Viabill_Block_Form extends Mage_Payment_Block_Form{
    
     protected function _construct()
    {
        $this->setTemplate('viabill/form.phtml');
        parent::_construct();
    }
    
    public function getDescription(){
        return trim(Mage::getStoreConfig('payment/epay_viabill/description'));
    }

    public function isShowIcon(){
        return Mage::getStoreConfig('payment/epay_viabill/show_icon');
    }
    
  
}