<?php
/**
 * @author Rune Kok Nielsen <rune@bixel.dk>
 */
class Bixel_Viabill_Block_Redirect extends Mage_Epay_Block_Standard_Redirect{

    public function __construct()
    {
        parent::__construct();
        $standard = Mage::getModel('epay/standard');

        $this->setTemplate('viabill/redirect_standardwindow.phtml');

        //
        // Save the order into the epay_order_status table
        //
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $write->insert('epay_order_status', Array('orderid'=>$standard->getCheckout()->getLastRealOrderId()));
    }

}