<?php
/**
 * @author Rune Kok Nielsen <rune@bixel.dk>
 */
class Bixel_Viabill_Model_Payment extends Mage_Epay_Model_Standard {
    
    protected $_code = 'epay_viabill';
    
    protected $_formBlockType = 'viabill/form';
    
    public function getTitle(){
        return Mage::getStoreConfig('payment/epay_viabill/title');
    }
    
    public function createFormBlock($name)
    {
        $block = $this->getLayout()->createBlock('epay/standard_form', $name)->setMethod('epay_viabill')->setPayment($this->getPayment())->setTemplate('epay/viabill/form.phtml');

        return $block;
    }
    
    public function getOrderPlaceRedirectUrl(){
        return Mage::getUrl('viabill/standard/redirect');
    }
    

    public function getConfigData($field, $storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->getStore();
        }
        $path = 'payment/'.$this->getCode().'/'.$field;
        $ownField = Mage::getStoreConfig($path, $storeId);
        
        if($ownField===null) return Mage::getStoreConfig('payment/epay_standard/'.$field);
        return $ownField;
    }
}
