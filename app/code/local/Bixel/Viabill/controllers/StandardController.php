<?php
/**
 * @author Rune Kok Nielsen <rune@bixel.dk>
 */
include('app/code/local/Mage/Epay/controllers/StandardController.php');
class Bixel_Viabill_StandardController extends Mage_Epay_StandardController{
    
     /**
     * When a customer chooses Epay on Checkout/Payment page
     *
     */
    public function redirectAction()
    {
        //
        // Load layout
        //
        $this->loadLayout();
        $this->getLayout()->getBlock('content')->append($this->getLayout()->createBlock('viabill/redirect'));
        $this->renderLayout();

        //
        // Load teh session object
        //
        $session = Mage::getSingleton('checkout/session');
        $session->setEpayStandardQuoteId($session->getQuoteId());

        //
        // Save order comment
        //
        $this->_orderObj = Mage::getModel('sales/order');
        $this->_orderObj->loadByIncrementId($session->getLastRealOrderId());
        $this->_orderObj->addStatusToHistory($this->_orderObj->getStatus(), $this->__('EPAY_LABEL_31'));
        $this->_orderObj->save();
    }
    
    
}